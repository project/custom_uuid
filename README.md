## Create Entity with UUID
The Create Entity with UUID modules allows the users to add the custom UUID for
Block Content and Media Entity as of now. This would be useful for developers
and site builders to create a block/media with the mentioned UUID in 
configuration. This module introduces a new field Custom UUID to add the custom
UUID. Leave it empty if don't want a custom UUID, it will create entity with
default Drupal UUID.

## Table of Contents
- Requirements
- Installation
- Configuration
- Maintainers

## Requirements
- This module requires Media and Block Content module and it's dependency
  modules (available in Drupal Core) as requirement.

## Installation
- Install as you would normally install a contributed Drupal module. For further
  information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
- This module doesn't have any configuration as of now. When the module enabled
  it provides additional field to add the UUID in Add media and
  Add Custom/Content Block form which validates and create the entity with
  provided UUID.

## Maintainers
-  Sakthi Shanmuga Sundaram (sakthi_dev)